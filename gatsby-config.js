module.exports = {
    plugins: [
      {
        resolve: `@kentico/gatsby-source-kontent`,
        options: {
          deliveryClientConfig: {
            projectId: `<PROJECTID>`,
            typeResolvers: [],
            previewApiKey: `<PRIMARYKEY>`,
            secureApiKey: ``,
            globalQueryConfig: {
              usePreviewMode: true, // uses preview mode
              useSecuredMode: false, // disabled secured mode
            },
          },
          languageCodenames: [`en-GB`],
        },
      },
    ],
  };