var slugify = require("slugify");
var createModulePage = require("./gatsby-node-create-page-module");

exports.createPages = async function ({ actions, graphql }) {
  const { createPage } = actions;

  const {
    data: { allKontentTypeHomepage: homePage },
  } = await graphql(`
    query MyQuery {
      allKontentTypeHomepage {
        nodes {
          contentItems {
            elements {
              title {
                value
              }
            }
          }
        }
      }
    }
  `);

  homePage.nodes[0].contentItems.forEach((pageData) =>
    createModulePage(pageData, { homePage: true }, actions)
  );
};

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /document-offset/,
            use: loaders.null(),
          },
        ],
      },
    });
  }
};
