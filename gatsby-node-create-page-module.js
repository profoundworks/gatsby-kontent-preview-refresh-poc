var slugify = require("slugify");
var HomepageTemplate = require.resolve(`./src/templates/homepage`);

module.exports = (pageData, globalData, { createPage }) => {
  const { homePage = false } = globalData;

  let path = "";

  createPage({
    path: "/",
    component: homePage ? HomepageTemplate : null,
    context: {
      pageData: pageData.elements,
    },
  });
};
